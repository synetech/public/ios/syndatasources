//
//  KeychainKeys.swift
//  AuthKeychainTest
//
//  Created by Vojtěch Pajer on 07/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import SYNStringRepresentable

enum KeychainKey: String, StringRepresentable {
    case first
    case second
    case third
}
