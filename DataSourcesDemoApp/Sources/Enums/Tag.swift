//
//  ButtonTag.swift
//  AuthKeychainTest
//
//  Created by Vojtěch Pajer on 07/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import SYNAuthProtectedStorage

enum Tag: Int {
    case first = 1
    case second
    case third
    
    var keychainKey: KeychainKey {
        switch self {
        case .first:
            return .first
        case .second:
            return .second
        case .third:
            return .third
        }
    }
    
    var authPolicy: AuthenticationPolicy {
        switch self {
        case .first:
            return .passcodeOnly
        case .second:
            return .biometryOnly
        case .third:
            return .biometryAndPasscode
        }
    }
    
    var placeholder: String {
        switch self {
        case .first:
            return "Passcode only authentication"
        case .second:
            return "Biometry only authentication"
        case .third:
            return "Biometry and passcode authentication"
        }
    }
    
    var prompt: String {
        switch self {
        case .first:
            return "Hello everyone, my name is Milos, but my friends call me the Z-man."
        case .second:
            return "Let me tell you something about my smoke and becherovka filled life."
        case .third:
            return "Oh wait, I just realized I don't care about your existence. Bye."
        }
    }
}
