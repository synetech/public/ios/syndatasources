//
//  AuthProtectedKeychainRepositoryImpl.swift
//  AuthKeychainTest
//
//  Created by Vojtěch Pajer on 07/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import SYNAuthProtectedStorage
import KeychainAccess

class AuthProtectedKeychainRepositoryImpl: AuthProtectedKeychainStorage<KeychainKey> {}
