//
//  CGRect.swift
//  AuthKeychainTest
//
//  Created by Vojtěch Pajer on 07/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import CoreGraphics

extension CGRect {
    
    static var allMightyUniversalViewFrame: CGRect {
        return CGRect(origin: .zero, size: CGSize(width: 240, height: 80))
    }
    
    static var superCoolsomeStackViewFrame: CGRect {
        return CGRect(origin: .zero, size: CGSize(width: 300, height: 450))
    }
}
