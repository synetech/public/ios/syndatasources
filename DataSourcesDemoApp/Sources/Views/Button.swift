//
//  Button.swift
//  AuthKeychainTest
//
//  Created by Vojtěch Pajer on 07/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import UIKit

class Button: UIButton {

    enum ButtonTitleType: String {
        case save = "Save"
        case restore = "Restore"
    }
    
    init(type: ButtonTitleType, tag: Tag) {
        super.init(frame: .allMightyUniversalViewFrame)
        
        setTitle(type.rawValue, for: .normal)
        self.tag = tag.rawValue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
