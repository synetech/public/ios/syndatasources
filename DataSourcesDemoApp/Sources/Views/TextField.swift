//
//  TextField.swift
//  AuthKeychainTest
//
//  Created by Vojtěch Pajer on 07/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import UIKit

class TextField: UITextField {
    
    init(tag: Tag) {
        super.init(frame: .allMightyUniversalViewFrame)
        
        backgroundColor = .white
        returnKeyType = .done
        textAlignment = .center
        adjustsFontSizeToFitWidth = true
        
        placeholder = tag.placeholder
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 10)
    }
}

