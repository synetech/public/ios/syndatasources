//
//  AuthViewController.swift
//  AuthKeychainTest
//
//  Created by Vojtěch Pajer on 07/11/2019.
//  Copyright © 2019 Vojtěch Pajer. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {
    
    // MARK: - Views
    private let tfSecret1 = TextField(tag: .first)
    private let btnSave1 = Button(type: .save, tag: .first)
    private let btnRestore1 = Button(type: .restore, tag: .first)
    
    private let tfSecret2 = TextField(tag: .second)
    private let btnSave2 = Button(type: .save, tag: .second)
    private let btnRestore2 = Button(type: .restore, tag: .second)
    
    private let tfSecret3 = TextField(tag: .third)
    private let btnSave3 = Button(type: .save, tag: .third)
    private let btnRestore3 = Button(type: .restore, tag: .third)
    
    private lazy var svContent: UIStackView = {
        let stackView = UIStackView(frame: .superCoolsomeStackViewFrame)
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        return stackView
    } ()
    
    
    // MARK: - Properties
    private let keychainRepository = AuthProtectedKeychainRepositoryImpl()
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    

    // MARK: - Setup
    private func setupViews() {
        view.backgroundColor = .black
        
        addSubviews(views: tfSecret1, btnSave1, btnRestore1, tfSecret2,
                    btnSave2, btnRestore2, tfSecret3, btnSave3, btnRestore3)
        setupTextFields(textFields: tfSecret1, tfSecret2, tfSecret3)
        setupSaveButtons(saveButtons: btnSave1, btnSave2, btnSave3)
        setupRestoreButtons(restoreButtons: btnRestore1, btnRestore2, btnRestore3)
        
        view.addSubview(svContent)
        svContent.center = view.center
    }
}

// MARK: - Private
private extension AuthViewController {
    
    func addSubviews(views: UIView...) {
        views.forEach { svContent.addArrangedSubview($0) }
     }
    
    func setupTextFields(textFields: UITextField...) {
        textFields.forEach { $0.delegate = self }
    }
    
    func setupSaveButtons(saveButtons: UIButton...) {
        saveButtons.forEach { $0.addTarget(self,
                                           action: #selector(saveTapped),
                                           for: .touchUpInside) }
    }
    
    func setupRestoreButtons(restoreButtons: UIButton...) {
        restoreButtons.forEach { $0.addTarget(self,
                                              action: #selector(restoreTapped),
                                              for: .touchUpInside) }
    }
    
    @objc
    func saveTapped(button: UIButton) {
        let buttonTag = button.tag
        let tagType = Tag(rawValue: buttonTag) ?? .first

        Task {
            await keychainRepository
                .saveAuthProtectedString(getTextField(forTag: buttonTag).text ?? "",
                                         key: tagType.keychainKey,
                                         authPolicy: tagType.authPolicy)
            getTextField(forTag: buttonTag).text = ""
        }
    }
    
    @objc
    func restoreTapped(button: UIButton) {
        let buttonTag = button.tag
        let tagType = Tag(rawValue: buttonTag) ?? .first

        Task {
            let token = try await keychainRepository.loadAuthProtectedString(key: tagType.keychainKey,
                                                                             authenticationPrompt: tagType.prompt)
            showAlertController(text: token ?? "")
        }
    }
    
    func getTextField(forTag tag: Int) -> UITextField {
        switch tag {
        case Tag.first.rawValue:
            return tfSecret1
        case Tag.second.rawValue:
            return tfSecret2
        case Tag.third.rawValue:
            return tfSecret3
        default:
            return UITextField()
        }
    }
    
    func showAlertController(text: String) {
        let message = text.isEmpty ? "You have not saved anything, yet." : text
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

extension AuthViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
