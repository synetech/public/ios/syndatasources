## 1.0.0 Release notes (11-11-2022)

### Breaking Changes
- `Database` and `DatabaseObject` target moved to the `SYNAncient` library
- Removed `RxSwift` dependency
- `AuthProtectedKeychainStorage` refactored to handle asynchronicity using Swift concurrency
- Targets renamed to have `SYN` prefix and don't have `DataSource` suffix


## 0.1.2 Release notes (7-10-2021)

### Enhancements
- RxSwift raised to 6.x, KeychainAccess raised to 4.2.1.


## 0.1.1 Release notes (30-07-2020)

### Bugfixes 
- `RealmDatabaseImpl` init extended to make possible to use persisted database.


## 0.1.0 Release notes (29-07-2020)

### Breaking Changes

- Added Database data source and RealmDatabaseInteractor.


## 0.0.6 Release notes (07-01-2020)

### Bugfixes 

* Changed target names so that the package can be used properly.
* Changed SecuredStorage from protocol to struct so that it can be used properly. 


## 0.0.5 Release notes (06-01-2020)

### Bugfixes 

* Fixed accesibility of initializers and class properties.


## 0.0.4 Release notes (06-01-2020)

### Bugfixes 

* Changed KeyValueStorage from protocol to struct so that it can be used properly.


## 0.0.3 Release notes (06-01-2020)

### Bugfixes 

* Changed KeyValueStorage and SecuredStorage repositories to DS and renamed them accordingly. 


## 0.0.2 Release notes (02-01-2020)

### Enhancements

* Added `UserDefaults` target. 
* Added `Keychain` target. 


## 0.0.1 Release notes (28-10-2019)

### Breaking Changes
- Init.
