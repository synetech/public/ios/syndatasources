// swift-tools-version:5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

enum PackageProduct: String, CaseIterable {

    case wholePackage = "SYNDataSources"

    var name: String {
        return rawValue
    }

    var product: Product {
        return .library(name: rawValue, targets: targets)
    }

    var targets: [String] {
        switch self {
        case .wholePackage:
            return PackageTarget.allCases.map { $0.name }
        }
    }
}

enum PackageDependency: String, CaseIterable {

    case keychainAccess = "KeychainAccess"

    var dependency: Target.Dependency {
        return Target.Dependency(stringLiteral: rawValue)
    }

    static var packages: [Package.Dependency] {
        return [
            .package(url: "https://github.com/kishikawakatsumi/KeychainAccess", from: "4.2.2")
        ]
    }
}

enum PackageTarget: String, CaseIterable {

    case authProtectedStorage = "SYNAuthProtectedStorage"
    case keyValueStorage = "SYNKeyValueStorage"
    case securedStorage = "SYNSecuredStorage"
    case stringRepresentable = "SYNStringRepresentable"

    var name: String {
        return rawValue
    }

    var dependency: Target.Dependency {
        return Target.Dependency(stringLiteral: rawValue)
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .authProtectedStorage:
            return [PackageTarget.securedStorage.dependency]
        case .keyValueStorage:
            return [PackageTarget.stringRepresentable.dependency]
        case .securedStorage:
            return [PackageTarget.stringRepresentable.dependency, PackageDependency.keychainAccess.dependency]
        case .stringRepresentable:
            return []
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.target(name: $0.name, dependencies: $0.dependencies) }
    }
}

enum PackageTestTarget: String, CaseIterable {

    case authProtectedStorage = "AuthProtectedStorageTests"
    case keyValueStorage = "KeyValueStorageTests"
    case securedStorage = "SecuredStorageTests"
    case stringRepresentable = "StringRepresentableTests"

    var name: String {
        return rawValue
    }

    var dependencies: [Target.Dependency] {
        switch self {
        case .authProtectedStorage:
            return [PackageTarget.authProtectedStorage.dependency]
        case .securedStorage:
            return [PackageTarget.stringRepresentable.dependency, PackageTarget.securedStorage.dependency]
        case .stringRepresentable:
            return [PackageTarget.stringRepresentable.dependency]
        case .keyValueStorage:
            return [PackageTarget.stringRepresentable.dependency, PackageTarget.keyValueStorage.dependency]
        }
    }

    static var targets: [Target] {
        return allCases.map { Target.testTarget(name: $0.name, dependencies: $0.dependencies) }
    }
}

let package = Package(
    name: "SYNDataSources",
    platforms: [.iOS(.v13), .macOS(.v10_15)],
    // Products define the executables and libraries produced by a package, and make them visible to other packages.
    products: PackageProduct.allCases.map { $0.product },
    // Dependencies declare other packages that this package depends on.
    dependencies: PackageDependency.packages,
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages which this package depends on.
    targets: PackageTarget.targets + PackageTestTarget.targets
)
