//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 05/11/2019.
//

import Foundation
import SYNStringRepresentable

open class KeyValueStorage<Key: StringRepresentable> {
    
    /// A generic save method that will take any object that can be saved into the key-value store and stores it
    ///
    /// - Parameters:
    ///   - object: object to be stored
    ///   - key: key under which will be the object stored
    ///
    public func save<T>(object: T, key: Key) {}
    
    /// A  get method to retrieve a stored values in user defaults (key-value) store if it exists
    ///
    /// - Parameter key: key under which is the object stored
    /// - Returns: `Any` object or nil
    public func get(key: Key) -> Any? {
        return nil
    }

    /// A generic get method to retrieve a stored values in user defaults (key-value) store if exists
    /// This method will return the stored object or a default value
    ///
    /// - Parameters:
    ///   - key: key under which is the object stored
    ///   - defaultValue: default value that will be returned if the object does not exist in the store
    /// - Returns: object or default value
    public func get<T>(key: Key, defaultValue: T) -> T {
        return defaultValue
    }

    /// A generic remove to remove the object from the storage
    ///
    /// - Parameter key: key under which is the object stored
    public func remove(key: Key) {}
}
