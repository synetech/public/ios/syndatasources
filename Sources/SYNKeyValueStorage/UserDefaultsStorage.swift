//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 06/01/2020.
//

import Foundation
import SYNStringRepresentable

/// Class that takes care of the UserDefaults interaction.
///
/// The recommended usage has following steps:
///  1. Create the `DefaultsKey` enum representing the UserDefaults keys that conforms to `StringRepresentable` protocol (see `StringRepresentable` documentation)
///  2. Create a `UserDefaultsImpl` file that defines key value storage typealiases and specifies their generic type:
///     ```
///     import KeyValueStorageDataSource
///
///     typealias GenericKeyValueStorage = KeyValueStorageDataSource.KeyValueStorage
///     typealias KeyValueStorage = KeyValueStorageDataSource.KeyValueStorage<DefaultsKey>
///     typealias UserDefaultsStorageImpl = UserDefaultsStorage<DefaultsKey>
///     ```
///  3. Register the repository to the Dependency Injection `Container`, e.g.:
///     ```
///     container.register(GenericKeyValueStorage.self) { UserDefaultsStorageImpl() }
///     ```
///  4. Resolve it from the `Container` using the `KeyValueStorage` typealias:
///     ```
///     container.resolve(KeyValueStorage.self)!
///     ```
///  5. Enjoy the simple use of the `UserDefaults`.

open class UserDefaultsStorage<Key: StringRepresentable>: KeyValueStorage<Key> {
    
    // MARK: - Properties
    private let defaults: UserDefaults
    
    // MARK: - Init
    public init(suiteName: String) {
        defaults = UserDefaults(suiteName: suiteName) ?? UserDefaults.standard
    }
    
    override public init() {
        defaults = UserDefaults.standard
    }
    
    // MARK: - Override functions
    override public func save<T>(object: T, key: Key) {
        defaults.set(object, forKey: key.rawValue)
        defaults.synchronize()
    }

    override public func get(key: Key) -> Any? {
        return defaults.object(forKey: key.rawValue)
    }

    override public func get<T>(key: Key, defaultValue: T) -> T {
        if let object: T = get(key: key) as? T {
            return object
        }
        return defaultValue
    }

    override public func remove(key: Key) {
        defaults.removeObject(forKey: key.rawValue)
    }
}
