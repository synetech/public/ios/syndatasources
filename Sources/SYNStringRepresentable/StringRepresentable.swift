//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 06/11/2019.
//

import Foundation

/// Enum of known keys that can be used to store values in user defaults or keychain key-value data store
///
/// Recommended usage:
/// ```
/// enum DefaultsKeys: StringRepresentable {
///    case isFirstRun
///    case networkingCache(endpoint: String)
///
///    var rawValue: String {
///        switch self {
///        case .isFirstRun:
///            return "isFirstRun"
///        case .networkingCache(let endpoint):
///            return "networkingCache_\(endpoint)"
///        }
///    }
/// }
/// ```

public protocol StringRepresentable {
    
    var rawValue: String { get }
}
