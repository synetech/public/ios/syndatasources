//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 07/11/2019.
//

import KeychainAccess

/// Policy specifying authentication options for access to the Keychain
public enum AuthenticationPolicy {
    
    /// User can authenticate him/her self only by his/her passcode.
    case passcodeOnly
    
    /// User can authenticate him/her self only by his/her TouchID or FaceID.
    case biometryOnly
    
    /// User can authenticate him/her self either by his/her passcode or his/her TouchID or FaceID.
    /// - Note: This is a default value.
    case biometryAndPasscode
    
    var policy: KeychainAccess.AuthenticationPolicy {
        switch self {
        case .passcodeOnly:
            return .devicePasscode
        case .biometryOnly:
            if #available(OSX 10.13.4, iOS 11.3, *) {
                return .biometryAny
            } else {
                return .touchIDAny
            }
        case .biometryAndPasscode:
            if #available(OSX 10.13.4, iOS 11.3, *) {
                return [.biometryAny, .or, .devicePasscode]
            } else {
                return .userPresence
            }
        }
    }
}
