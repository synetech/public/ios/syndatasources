//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 06/11/2019.
//

import Foundation
import SYNSecuredStorage
import SYNStringRepresentable

/// The repository that takes care of the Keychain interaction when saving keys that require authentication.
///
/// The recommended usage has following steps:
///  1. Create the `KeychainKey` enum representing the Keychain keys that conforms to `StringRepresentable` protocol (see `StringRepresentable` documentation)
///  2. Create a `AuthProtectedKeychainStorageImpl` class with nothing but the `Key` associated value assignment and keychain property definition:
///     ```
///     class AuthProtectedKeychainStorageImpl: AuthProtectedKeychainStorage {
///         associatedtype Key = KeychainKey
///
///         let keychain = Keychain(service: "https://www.synetech.cz")
///     }
///     ```
///  3. When using FaceID to authenticate, add the `Privacy - Face ID Usage Description`key to your Info.plist
///  4. Register the repository to the Dependency Injection `Container`, e.g.:
///     ```
///     container.register(AuthProtectedKeychainStorage.self) { AuthProtectedKeychainStorageImpl() }
///     ```
///  5. Enjoy the simple use of the `Keychain`.

open class AuthProtectedKeychainStorage<Key: StringRepresentable>: KeychainStorage<Key> {}

// MARK: - Public String extension
public extension AuthProtectedKeychainStorage {

    func saveAuthProtectedString(_ string: String, key: Key) async {
        await saveString(string, key: key)
    }

    func saveAuthProtectedString(_ string: String, key: Key, authPolicy: AuthenticationPolicy) async {
        await saveString(string, key: key, authPolicy: authPolicy)
    }

    func updateAuthProtectedString(_ string: String, key: Key, authenticationPrompt: String) async {
        await saveString(string, key: key, authenticationPrompt: authenticationPrompt)
    }

    func updateAuthProtectedString(_ string: String, key: Key, authenticationPrompt: String, authPolicy: AuthenticationPolicy) async {
        await saveString(string, key: key, authenticationPrompt: authenticationPrompt, authPolicy: authPolicy)
    }

    func loadAuthProtectedString(key: Key) async throws -> String? {
        try await loadAuthProtectedString(key: key, authenticationPrompt: "")
    }

    func loadAuthProtectedString(key: Key, authenticationPrompt: String) async throws -> String? {
        try await withCheckedThrowingContinuation { continuation in
            do {
                let string = try keychain
                    .authenticationPrompt(authenticationPrompt)
                    .getString(key.rawValue)
                continuation.resume(with: .success(string))
            } catch {
                continuation.resume(with: .failure(error))
            }
        }
    }
}

// MARK: - Public Data extension
public extension AuthProtectedKeychainStorage {

    func saveAuthProtectedData(_ data: Data?, key: Key) async {
        await saveData(data, key: key)
    }

    func saveAuthProtectedData(_ data: Data?, key: Key, authPolicy: AuthenticationPolicy) async {
        await saveData(data, key: key, authPolicy: authPolicy)
    }

    func updateAuthProtectedData(_ data: Data?, key: Key, authenticationPrompt: String) async {
        await saveData(data, key: key, authenticationPrompt: authenticationPrompt)
    }

    func updateAuthProtectedData(_ data: Data?, key: Key, authenticationPrompt: String, authPolicy: AuthenticationPolicy) async {
        await saveData(data, key: key, authenticationPrompt: authenticationPrompt, authPolicy: authPolicy)
    }

    func loadAuthProtectedData(key: Key) async throws -> Data? {
        try await loadAuthProtectedData(key: key, authenticationPrompt: "")
    }

    func loadAuthProtectedData(key: Key, authenticationPrompt: String) async throws -> Data? {
        try await withCheckedThrowingContinuation { continuation in
            do {
                let data = try keychain
                    .authenticationPrompt(authenticationPrompt)
                    .getData(key.rawValue)
                continuation.resume(with: .success(data))
            } catch {
                continuation.resume(with: .failure(error))
            }
        }
    }
}

// MARK: - Private
private extension AuthProtectedKeychainStorage {

    func saveString(_ string: String, key: Key, authenticationPrompt: String = "", authPolicy: AuthenticationPolicy = .biometryAndPasscode) async {
        await withCheckedContinuation { continuation in
            keychain
                .accessibility(.whenUnlocked, authenticationPolicy: authPolicy.policy)
                .authenticationPrompt(authenticationPrompt)[string: key.rawValue] = string
            continuation.resume(with: .success(Void()))
        }
    }

    func saveData(_ data: Data?, key: Key, authenticationPrompt: String = "", authPolicy: AuthenticationPolicy = .biometryAndPasscode) async {
        await withCheckedContinuation { continuation in
            keychain
                .accessibility(.whenUnlocked, authenticationPolicy: authPolicy.policy)
                .authenticationPrompt(authenticationPrompt)[data: key.rawValue] = data
            continuation.resume(with: .success(Void()))
        }
    }
}
