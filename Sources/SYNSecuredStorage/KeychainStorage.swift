//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 06/01/2020.
//

import Foundation
import KeychainAccess
import SYNStringRepresentable

/// Class that takes care of the Keychain interaction.
///
/// The recommended usage has following steps:
///  1. Create the `KeychainKey` enum representing the Keychain keys that conforms to `StringRepresentable` protocol (see `StringRepresentable` documentation)
///  2. Create a `KeychainStorageImpl` file that defines secured storage typealiases and specifies their generic type:
///     ```
///     import SecuredStorageDataSource
///
///     typealias GenericSecuredStorage = SecuredStorageDataSource.SecuredStorage
///     typealias SecuredStorage = SecuredStorageDataSource.SecuredStorage<KeychainKey>
///     typealias KeychainStorageImpl = KeychainStorage<KeychainKey>
///     ```
///  3. Register the repository to the Dependency Injection `Container`, e.g.:
///     ```
///     container.register(GenericSecuredStorage.self) { KeychainStorageImpl() }
///     ```
///  4. Resolve it from the `Container` using the `SecuredStorage` typealias:
///     ```
///     container.resolve(SecuredStorage.self)!
///     ```
///  5. Enjoy the simple use of the `Keychain`.

open class KeychainStorage<Key: StringRepresentable>: SecuredStorage<Key> {
    
    // MARK: - Properties
    public let keychain: Keychain
    
    // MARK: - Init
    public init(service: String, accessGroup: String) {
        keychain = Keychain(service: service, accessGroup: accessGroup)
    }
    
    public init(service: String) {
        keychain = Keychain(service: service)
    }
    
    public init(accessGroup: String) {
        keychain = Keychain(accessGroup: accessGroup)
    }
    
    override public init() {
        keychain = Keychain()
    }
    
    // MARK: - Override functions
    override public func save(string: String, key: Key) {
        keychain[string: key.rawValue] = string
    }
    
    override public func save(data: Data?, key: Key) {
        keychain[data: key.rawValue] = data
    }
    
    override public func loadString(key: Key) -> String? {
        return keychain[string: key.rawValue]
    }
    
    override public func loadData(key: Key) -> Data? {
        return keychain[data: key.rawValue]
    }
    
    override public func delete(key: Key) {
        keychain[key.rawValue] = nil
    }
    
    override public func deleteAll() {
        try? keychain.removeAll()
    }
}
