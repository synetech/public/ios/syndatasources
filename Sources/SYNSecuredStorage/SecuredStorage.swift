//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 06/11/2019.
//

import Foundation
import SYNStringRepresentable

open class SecuredStorage<Key: StringRepresentable> {
    
    /// A  method to save a string value under specified key to the secured storage.
    ///
    /// - Parameters:
    ///   - string: string to be stored
    ///   - key: key under which will be the string stored
    ///
    public func save(string: String, key: Key) {}
    
    /// A  method to save data under specified key to the secured storage.
    ///
    /// - Parameters:
    ///   - data: data to be stored
    ///   - key: key under which will be the data stored
    ///
    public func save(data: Data?, key: Key) {}
    
    /// A method to retrieve a stored string from the secured storage (if it exists).
    ///
    /// - Parameters:
    ///   - key: key under which is the string stored
    /// - Returns: string or nil
    public func loadString(key: Key) -> String? {
        return nil
    }
    
    /// A method to retrieve a stored data from the secured storage (if it exists).
    ///
    /// - Parameters:
    ///   - key: key under which are the data stored
    /// - Returns: data or nil
    public func loadData(key: Key) -> Data? {
        return nil
    }
    
    /// A method to remove object under given key from the secured storage
    ///
    /// - Parameter key: key under which is the object stored
    public func delete(key: Key) {}
    
    /// A method to remove all objects from the secured storage
    public func deleteAll() {}
}
