# SYN Data Sources

Package of handy data sources useful for the majority of applications.  

## Targets

Each target can be imported separately or all targets can be imported at once by importing the whole Package. 

### User Defaults
Repository simplifying the usage of `UserDefaults`. 

### Keychain 
Repository simplifying the usage of `Keychain`. 

### AuthProtectedKeychain
Repository simplifying the usage of `Keychain` with authentication.

See the sample project on how to use it here: 
https://gitlab.com/synetech/public/ios/example-projects/authprotectedkeychain
