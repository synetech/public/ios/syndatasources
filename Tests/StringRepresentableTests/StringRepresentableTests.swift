//
//  File.swift
//  
//
//  Created by Vojtěch Pajer on 06/11/2019.
//

import XCTest
@testable import SYNStringRepresentable

final class StringRepresentableTests: XCTestCase {

    enum DefaultsKey: StringRepresentable {
        case isFirstRun
        case phoneColor(type: String)
        case apartment(street: String, number: Int)
        
        var rawValue: String {
            switch self {
            case .isFirstRun:
                return "isFirstRun"
            case .phoneColor(let type):
                return "\(type)_phoneColor"
            case .apartment(let street, let number):
                return "apartment_\(street)_\(number)"
            }
        }
    }
    
    func testKeysName() {
        XCTAssertEqual(DefaultsKey.isFirstRun.rawValue, "isFirstRun")
        XCTAssertEqual(DefaultsKey.phoneColor(type: "Nokia").rawValue, "Nokia_phoneColor")
        XCTAssertEqual(DefaultsKey.apartment(street: "Drmaly", number: 76).rawValue, "apartment_Drmaly_76")
    }
}
