//
//  File.swift
//
//
//  Created by Vojtěch Pajer on 05/11/2019.
//

import XCTest
@testable import SYNKeyValueStorage
@testable import SYNStringRepresentable

final class UserDefaultsTests: XCTestCase {
    
    enum DefaultsKey: StringRepresentable {
        case isFirstRun
        case phoneNumber
        case phoneColor(type: String)
        case apartment(street: String, number: Int)
        
        var rawValue: String {
            switch self {
            case .isFirstRun:
                return "isFirstRun"
            case .phoneNumber:
                return "phoneNumber"
            case .phoneColor(let type):
                return "\(type)_phoneColor"
            case .apartment(let street, let number):
                return "apartment_\(street)_\(number)"
            }
        }
    }
    
    final class UserDefaultsStorageImpl: UserDefaultsStorage<DefaultsKey> {}
    
    func testSaveAndGet() {
        let defaultsRepository = UserDefaultsStorageImpl(suiteName: "testSaveAndGet")
        
        defaultsRepository.save(object: true, key: .isFirstRun)
        XCTAssertEqual(defaultsRepository.get(key: .isFirstRun, defaultValue: false), true)
        XCTAssertEqual(defaultsRepository.get(key: .isFirstRun, defaultValue: ""), "")
        let isFirstRunBool: Bool? = defaultsRepository.get(key: .isFirstRun) as? Bool
        let isFirstRunString: String? = defaultsRepository.get(key: .isFirstRun) as? String
        XCTAssertEqual(isFirstRunBool, true)
        XCTAssertEqual(isFirstRunString, nil)
        
        defaultsRepository.save(object: 1234, key: .phoneNumber)
        XCTAssertEqual(defaultsRepository.get(key: .phoneNumber, defaultValue: 0), 1234)
        XCTAssertEqual(defaultsRepository.get(key: .phoneNumber, defaultValue: ""), "")
        let phoneNumberInt: Int? = defaultsRepository.get(key: .phoneNumber) as? Int
        let phoneNumberString: String? = defaultsRepository.get(key: .phoneNumber) as? String
        XCTAssertEqual(phoneNumberInt, 1234)
        XCTAssertEqual(phoneNumberString, nil)
        
        defaultsRepository.save(object: "green", key: .phoneColor(type: "Nokia"))
        defaultsRepository.save(object: "blue", key: .phoneColor(type: "Samsung"))
        XCTAssertEqual(defaultsRepository.get(key: .phoneColor(type: "Nokia"), defaultValue: ""), "green")
        XCTAssertEqual(defaultsRepository.get(key: .phoneColor(type: "Nokia"), defaultValue: 123), 123)
        XCTAssertEqual(defaultsRepository.get(key: .phoneColor(type: "Samsung"), defaultValue: ""), "blue")
        XCTAssertEqual(defaultsRepository.get(key: .phoneColor(type: "iPhone"), defaultValue: ""), "")
        
        let phoneColorString: String? = defaultsRepository.get(key: .phoneColor(type: "Samsung")) as? String
        let phoneColorBool: Bool? = defaultsRepository.get(key: .phoneColor(type: "Samsung")) as? Bool
        XCTAssertEqual(phoneColorString, "blue")
        XCTAssertEqual(phoneColorBool, nil)
        
        XCTAssertEqual(defaultsRepository.get(key: .apartment(street: "ABC", number: 12), defaultValue: ""), "")
        let apartment: String? = defaultsRepository.get(key: .apartment(street: "ABC", number: 12)) as? String
        XCTAssertEqual(apartment, nil)
    }
    
    func testDelete() {
        let defaultsRepository = UserDefaultsStorageImpl()
        
        defaultsRepository.save(object: true, key: .isFirstRun)
        XCTAssertEqual(defaultsRepository.get(key: .isFirstRun, defaultValue: false), true)
        defaultsRepository.remove(key: .isFirstRun)
        XCTAssertEqual(defaultsRepository.get(key: .isFirstRun, defaultValue: false), false)
        
        let isFirstRun: Bool? = defaultsRepository.get(key: .isFirstRun) as? Bool
        XCTAssertEqual(isFirstRun, nil)
        
        defaultsRepository.remove(key: .isFirstRun)
    }
}
